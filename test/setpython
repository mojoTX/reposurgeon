#!/bin/sh
#
# setpython - create a local link from 'python' to a specified version
#
# This script is used to to redirect the 'python' in reposurgeon's
# shebang line to a specified version when running regression tests.

if [ -z "$1" ]
then
	if [ -e python ]
	then
		readlink python
	else
		command -v python
	fi
elif [ $1 = "python" ]
then
	rm -f ./python
	echo "python -> `command -v python`"
elif [ $1 = python2 -o $1 = python3 ]
then
	p=`command -v $1`
	case $p in
		*/bin/*) ln -sf $p ./python; echo "python -> $p";;
		*)
			saved=`readlink ./python 2>/dev/null`
			rm -f ./python
			# Python2 prints version on stderr; Python3 on stdout.
			v=`{ python -V 2>&1; } | sed '/Python [23]\./{y/P/p/;s/ //;s/\..*$//;}'`
			if [ $1 = "$v" ]
			then
				echo "python -> `command -v python`"
			else
				[ -n "$saved" ] && ln -sf $saved ./python
				echo "setpython: no python binary" >&2; exit 1
			fi
			;;
	esac
else
	echo "setpython: unrecognized python version" >&2
	exit 1
fi

exit 0
